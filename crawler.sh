#!/bin/bash
if [ $1 == "" ] || [ $1 == "0" ];
then
    MAX_SITES=1000
else
    MAX_SITES=$1
fi

mkdir -p temp
mkdir -p statistics
mkdir -p sites

# STEP 1 - BFS ALGORITHMS TO CRAWL THE WEB
MAIN_ARRAY=("/wiki/Cloud_computing")
ARRAY_LEN=${#MAIN_ARRAY[@]}
MAIN_IND=0
while [ $MAIN_IND -lt $ARRAY_LEN ] && [ $MAIN_IND -le $MAX_SITES ]
do
    THIS_FILE=$(echo ${MAIN_ARRAY[$MAIN_IND]} | cut -d '"' -f 2)
    THIS_FILE=${THIS_FILE:6}

    echo "Downloading page from https://en.wikipedia.org/wiki/${THIS_FILE}"
    curl -s "https://en.wikipedia.org/wiki/${THIS_FILE}" > "sites/${THIS_FILE}.html"

    if [ $ARRAY_LEN -lt $MAX_SITES ];
    then
        echo "Fetching links..."
        TEMP_ARRAY=($(grep -o "\(href=\"\/wiki\/\w*\"\)" "sites/${THIS_FILE}.html"))

        echo "Adding links to the queue..."
        ADDED=0
        REPEATED=0
        for i in "${TEMP_ARRAY[@]}"
        do
            FOUND="n"
            for j in "${MAIN_ARRAY[@]}"
            do
                if [ ${i,,} == ${j,,} ];
                then
                    FOUND="y"
                    REPEATED=$((REPEATED + 1))
                    break
                fi
            done

            if [ "$FOUND" == "n" ];
            then
                MAIN_ARRAY+=($i)
                ARRAY_LEN=${#MAIN_ARRAY[@]}
                ADDED=$((ADDED + 1))

                if [ $ARRAY_LEN -ge $MAX_SITES ];
                then
                    echo "Queue limit reached. Downloading the rest of the pages from queue..."
                    break
                fi
            fi
        done
        echo "$ADDED links added | $REPEATED links repeated (discarted)"
    fi

    MAIN_IND=$((MAIN_IND + 1))
done

ARRAY_LEN=${#MAIN_ARRAY[@]}
MAIN_IND=0
while [ $MAIN_IND -lt $ARRAY_LEN ] && [ $MAIN_IND -le $MAX_SITES ]
do


    THIS_FILE=$(echo ${MAIN_ARRAY[$MAIN_IND]} | cut -d '"' -f 2)

    THIS_FILE=${THIS_FILE:6}

    echo "Treating file $THIS_FILE: getting words, treating and counting..."

    lynx -dump sites/"$THIS_FILE".html > temp/"$THIS_FILE".txt


    cat "temp/$THIS_FILE.txt" | tr -dc "[:alpha:] \-\/\_\.\n\r" | tr "[:upper:]" "[:lower:]" > "temp/$THIS_FILE.treated1.txt"

    for w in `cat temp/"$THIS_FILE".treated1.txt`; do echo "$w"; done > temp/"$THIS_FILE".separated.txt

    cp temp/"$THIS_FILE".separated.txt temp/"$THIS_FILE".treated2.txt

    sed -i "s/^file\/\/.*//g; s/^https\/\/.*//g; s/^http\/\/.*//g; s/^android-app\/\/.*//g; s/^-//g; s/^-//g; s/^-//g; s/^-//g; s/-$//g; s/,$//g; s/\.$//g; s/\.$//g; s/\.$//g; s/\/$//g; s/\.$//g; s/\.$//g; s/\.$//g; s/:$//g; s/\;$//g; /^$/d" "temp/$THIS_FILE.treated2.txt"

    # sorting the words for better counting algorithm
    sort "temp/$THIS_FILE.treated2.txt" -o "temp/$THIS_FILE.sorted.txt"
    lw=""
    count=0
    first="y"
    # count algorithm, considering words are sorted and treated
    for w in `cat "temp/$THIS_FILE.sorted.txt"`;
    do
        if [ $first == "y" ];
        then
            first="n"
            lw=$w
        fi

        if [ $w == $lw ];
        then
            count=$((count + 1))
        else
            echo "$lw => $count"
            count=1
        fi

        lw=$w
    done > "temp/$THIS_FILE.counted.txt"
    sed -i "\$a$lw => $count" "temp/$THIS_FILE.counted.txt"
    mv "temp/$THIS_FILE.counted.txt" "statistics/$THIS_FILE.txt"

    MAIN_IND=$((MAIN_IND + 1))
done

read input
if [ $input == "y" ] || [ $input == "Y" ];
then
    rm -r temp/*
fi
echo "completed..."
