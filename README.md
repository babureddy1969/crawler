# README #

This ia simple crawler written in BASH script to crawl n number of websites. This follows the Breath First model approach.

### How do I get set up? ###

* Install LYNX text browser
* Install CURL for bash shell
* Pull code from this repository and save it in under crawler dir in your local machine
* bash crawler.sh 1000 # will crawl the first 1000 unique websites 
* bash count.sh www will count the number of occurances of the string "www" and show the resuls on the screen

### Who do I talk to? ###

* babureddy1969@gmail.com
